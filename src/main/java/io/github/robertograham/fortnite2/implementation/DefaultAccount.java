package io.github.robertograham.fortnite2.implementation;

import io.github.robertograham.fortnite2.domain.Account;
import io.github.robertograham.fortnite2.domain.enumeration.Platform;

import javax.json.JsonObject;
import javax.json.bind.adapter.JsonbAdapter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

final class DefaultAccount implements Account {

    private final String accountId;
    private final String displayName;
    private final Map<Platform, String> externalNames;

    private DefaultAccount(final String accountId,
                           final String displayName,
                           final Map<Platform, String> externalNames) {
        this.accountId = accountId;
        this.displayName = displayName;
        this.externalNames = externalNames;
    }

    @Override
    public String accountId() {
        return accountId;
    }

    @Override
    public String displayName() {
        return displayName;
    }

    @Override
    public Map<Platform, String> externalNames() {
        return externalNames;
    }

    @Override
    public String toString() {
        return "DefaultAccount{" +
                "accountId='" + accountId + '\'' +
                ", displayName='" + displayName + '\'' +
                ", externalNames=" + externalNames +
                '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof DefaultAccount))
            return false;
        final var defaultAccount = (DefaultAccount) object;
        return Objects.equals(accountId, defaultAccount.accountId) &&
                Objects.equals(displayName, defaultAccount.displayName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, displayName);
    }

    enum Adapter implements JsonbAdapter<DefaultAccount, JsonObject> {

        INSTANCE;

        @Override
        public JsonObject adaptToJson(final DefaultAccount defaultAccount) {
            throw new UnsupportedOperationException();
        }

        @Override
        public DefaultAccount adaptFromJson(final JsonObject jsonObject) {
         //   System.out.println(jsonObject);
            var externalAuths = jsonObject.getJsonObject("externalAuths").asJsonObject();
            Map<Platform, String> map = new HashMap<>();
            externalAuths.keySet().forEach(k -> {
                var platform = Platform.ofCode(k);
                if (platform != null)
                    map.put(platform, externalAuths.getJsonObject(k).getString("externalDisplayName"));
            });
            return new DefaultAccount(
                    jsonObject.getString("id", null),
                    jsonObject.getString("displayName", null),
                    map
                    );
        }
    }
}
